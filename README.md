# Compilation

# Auto formattage du code avant les commits

Si ce n'est pas déjà fait, installez l'outil pour les pre-commits

```bash
pip install pre-commit
pre-commit install
```
Pour compiler :
```bash
cmake -B build -S . -GNinja
cmake --build build
```

Pour l'envoyer sur la board
```bash
cmake --build build -- flash
```
