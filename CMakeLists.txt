cmake_minimum_required(VERSION 3.20)

# ##########################
# Policies        #
# ##########################
cmake_policy(SET CMP0069 NEW)

if(${CMAKE_VERSION} VERSION_GREATER "3.23.0")
    cmake_policy(SET CMP0135 NEW)
endif()

# ##########################
# Modules        #
# ##########################
include(FetchContent)
include(CheckIPOSupported)
include(${CMAKE_SOURCE_DIR}/cmake/tools/utilities.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/toolchains/vcpkg.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/toolchains/python_venv.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/toolchains/nnvg.cmake)

find_package(gcc-arm-none-eabi REQUIRED)

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

# specify cross compilers and tools
set(CMAKE_C_COMPILER ${ARM_GCC_PATH}/arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER ${ARM_GCC_PATH}/arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER ${ARM_GCC_PATH}/arm-none-eabi-gcc)
set(CMAKE_AR ${ARM_GCC_PATH}/arm-none-eabi-ar)
set(CMAKE_OBJCOPY ${ARM_GCC_PATH}/arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP ${ARM_GCC_PATH}/arm-none-eabi-objdump)
set(SIZE ${ARM_GCC_PATH}/arm-none-eabi-size)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(PLATFORM_NAME "LwIP")
set(CMAKE_BUILD_RPATH_USE_ORIGIN ON)
set(CMAKE_CXX_ARCHIVE_CREATE "<CMAKE_AR> -crD <TARGET> <LINK_FLAGS> <OBJECTS>")
set(CMAKE_CXX_ARCHIVE_APPEND "<CMAKE_AR> -rD <TARGET> <LINK_FLAGS> <OBJECTS>")
set(CMAKE_CXX_ARCHIVE_FINISH "<CMAKE_RANLIB> -D <TARGET>")

# SOURCE_DATE_EPOCH should be set to the last commit timestamp, preferably with `git log -1 --pretty=%ct`
if(NOT DEFINED ENV{SOURCE_DATE_EPOCH})
    execute_process(
        COMMAND git log -1 --pretty=%ct
        OUTPUT_VARIABLE SOURCE_DATE_EPOCH_TMP
        OUTPUT_STRIP_TRAILING_WHITESPACE
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    )
    set(ENV{SOURCE_DATE_EPOCH} ${SOURCE_DATE_EPOCH_TMP})
endif()

string(TIMESTAMP C_TIME "%Y-%m-%dT%H:%M:%SZ" UTC)
message(STATUS "Build date: ${C_TIME}")

# project settings
project(Surimi C CXX ASM)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 11)

# Project Options
# Options configuration
set(LOG_LEVEL 1)
option(CI_BUILD "Is the build currently made by a CI tool" No)

set(REMOTE_FLASH_ADDRESS "" CACHE STRING "Address of a remote device to send remotes commands to")
set(REMOTE_FLASH_USER "" CACHE STRING "User to connect to a remote device to send remotes commands to")
set(JLINK_SERIAL_NUMBER "" CACHE STRING "Optional serial number of the JLink to select")

# Option validation
if(${LOG_LEVEL} LESS 0 OR ${LOG_LEVEL} GREATER 3)
    message(FATAL_ERROR "Invalid log level: ${LOG_LEVEL}\nThe log level must be between 0 (DEBUG) and 3 (FATAL)")
endif()

if(${LOG_LEVEL} EQUAL 0)
    message(STATUS "Log level: DEBUG")
elseif(${LOG_LEVEL} EQUAL 1)
    message(STATUS "Log level: INFO")
elseif(${LOG_LEVEL} EQUAL 2)
    message(STATUS "Log level: WARNING")
elseif(${LOG_LEVEL} EQUAL 2)
    message(STATUS "Log level: FATAL")
else()
    message(FATAL_ERROR "Invalid log level")
endif()

if(${BUILD_DDS_PUBLISH_EXAMPLE} AND ${BUILD_DDS_SUBSCRIBE_EXAMPLE})
    message(FATAL_ERROR "You can build either subscribe or publish example, but not both at the same time")
endif()

check_ipo_supported(RESULT ipo_supported OUTPUT ipo_error)

if(ipo_supported)
    message(STATUS "IPO / LTO enabled")
else()
    message(STATUS "IPO / LTO not supported")
endif()

# Uncomment for hardware floating point
add_compile_definitions(ARM_MATH_CM4;ARM_MATH_MATRIX_CHECK;ARM_MATH_ROUNDING)
add_compile_options(-mfloat-abi=hard -mfpu=fpv4-sp-d16)
add_link_options(-mfloat-abi=hard -mfpu=fpv4-sp-d16)

add_compile_options(-mcpu=cortex-m4 -mthumb -mthumb-interwork)
add_compile_options(-ffunction-sections -fdata-sections -fno-common -fmessage-length=0)
add_compile_options(--specs=nano.specs -specs=nosys.specs)

# uncomment to mitigate c++17 absolute addresses warnings
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-register")

# compiler optimisation flag for bare metal
# See https://arobenko.github.io/bare_metal_cpp/ for details
add_compile_options(-nostdlib -fno-exceptions -fno-unwind-tables $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>)

add_link_options(-Wl,-gc-sections,--print-memory-usage,-Map=${PROJECT_BINARY_DIR}/${PROJECT_NAME}.map)
add_link_options(-mcpu=cortex-m4 -mthumb -mthumb-interwork -specs=nano.specs -specs=nosys.specs)

set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/linker.ld)

add_link_options(-T ${LINKER_SCRIPT})

include_directories(Core/Inc
    Core/Inc/Tasks
    Core/Inc/Servo
    Core/Inc/motors
    Core/Inc/motors/tmc/ic/TMC5130
    Core/Inc/motors/tmc/helpers
    Core/Inc/motors/tmc/ramp
    SYSTEM Drivers/STM32F4xx_HAL_Driver/Inc
    SYSTEM Drivers/STM32F4xx_HAL_Driver/Inc/Legacy
    Middlewares/Third_Party/FreeRTOS/Source/include
    Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2
    Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F
)

if("x${CMAKE_BUILD_TYPE}x" STREQUAL "xx")
    message(STATUS "No build type selected, default is RelWithDebInfo")
    set(CMAKE_BUILD_TYPE "RelWithDebInfo")
endif ()

if("${CMAKE_BUILD_TYPE}" STREQUAL "Release")
    message(STATUS "Optimize a bit")
    set(COMPIL_OPTION -O2)
    set(COMPIL_DEFINITION)
elseif("${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo")
    message(STATUS "Optimize a bit with debug enabled")
    set(COMPIL_OPTION -O2 -g3 -fasynchronous-unwind-tables -fstack-clash-protection -fstack-protector-all)
    set(COMPIL_DEFINITION _FORTIFY_SOURCE=2)
elseif("${CMAKE_BUILD_TYPE}" STREQUAL "MinSizeRel")
    message(STATUS "Maximum optimization for size")
    set(COMPIL_OPTION -Oz)
    set(COMPIL_DEFINITION)
elseif("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
    message(STATUS "Minimal optimization, debug info included")
    set(COMPIL_OPTION -Og -g3 -fasynchronous-unwind-tables -fstack-clash-protection -fstack-protector-all)
    set(COMPIL_DEFINITION _FORTIFY_SOURCE=2)
else()
    message(FATAL_ERROR "Not even the default build type was detected. WTF ?!")
endif()

set(GENERATED_DIR ${PROJECT_BINARY_DIR}/generated/)
file(MAKE_DIRECTORY "${GENERATED_DIR}")

# Dependencies
add_subdirectory(${CMAKE_SOURCE_DIR}/cmake/dependencies)

# BEGIN: opencyphal idl generation
set(GENERATED_CYPHAL_DIR ${GENERATED_DIR}/opencyphal)
file(MAKE_DIRECTORY "${GENERATED_CYPHAL_DIR}")
create_dsdl_target(
    "cyphal-idl"
    c
    ${GENERATED_CYPHAL_DIR}
    ${CMAKE_SOURCE_DIR}/cyphal-idl/uavcan
    OFF
    little
    "as-needed"
    ON
)
# END: opencyphal idl generation

# BEGIN: Banner generation
add_custom_command(OUTPUT ${GENERATED_DIR}/welcome_banner.h
    DEPENDS ${CMAKE_SOURCE_DIR}/generate_banner.py
    ${CMAKE_SOURCE_DIR}/welcome_banner.txt
    COMMAND ${Python3_VENV_EXECUTABLE} ${CMAKE_SOURCE_DIR}/generate_banner.py --name welcome_banner ${CMAKE_SOURCE_DIR}/welcome_banner.txt ${GENERATED_DIR}/welcome_banner.h
    COMMENT "Generating banner message"
)
add_custom_command(OUTPUT ${GENERATED_DIR}/critical_error_banner.h
    DEPENDS ${CMAKE_SOURCE_DIR}/generate_banner.py
    ${CMAKE_SOURCE_DIR}/critical_error_banner.txt
    COMMAND ${Python3_VENV_EXECUTABLE} ${CMAKE_SOURCE_DIR}/generate_banner.py --name critical_error_banner ${CMAKE_SOURCE_DIR}/critical_error_banner.txt ${GENERATED_DIR}/critical_error_banner.h
    COMMENT "Generating banner message"
)

# END: Banner generation
set(PYTHON_GENERATED_SOURCES
    ${GENERATED_DIR}/welcome_banner.h
    ${GENERATED_DIR}/critical_error_banner.h
)

include_directories(Core/Inc
    SYSTEM Core/Inc/SEGGER
    SYSTEM Core/Inc/Config
    SYSTEM Drivers/CMSIS/Device/ST/STM32F4xx/Include
    SYSTEM Drivers/CMSIS/Include
    ${GENERATED_DIR}
)

set_property(SOURCE Core/Inc/SEGGER/SEGGER_RTT_ASM_ARMv7M.S APPEND PROPERTY COMPILE_OPTIONS "-x" "assembler-with-cpp")
set_property(SOURCE startup_stm32f446xx.s APPEND PROPERTY COMPILE_OPTIONS "-x" "assembler-with-cpp")

glob_file(
    OUTPUT
    SOURCES_SYSTEM
    INPUT_FILES
    "Middlewares/*.*"
    "Drivers/*.*"
)

glob_file(
    OUTPUT
    SOURCES_APPLI
    INPUT_FILES
    "Core/Inc/*.*"
    "Core/Src/*.*"
)

foreach(source_file IN LISTS SOURCES_APPLI)
    set_property(SOURCE ${source_file} APPEND_STRING PROPERTY COMPILE_FLAGS "-Wall -Wextra -Wformat -Wmissing-format-attribute -Werror=format-security")
    if (${CI_BUILD})
        set_property(SOURCE ${source_file} APPEND_STRING PROPERTY COMPILE_FLAGS " -Werror")
    endif ()
endforeach()

add_executable(${PROJECT_NAME}.elf
    ${SOURCES_SYSTEM}
    ${SOURCES_APPLI}
    ${LINKER_SCRIPT}
    ${PYTHON_GENERATED_SOURCES}
    startup_stm32f446xx.s
)

target_compile_definitions(${PROJECT_NAME}.elf PUBLIC
    USE_HAL_DRIVER
    STM32F446xx
    DEBUG
    # NUNAVUT
    NUNAVUT_ASSERT=assert
    ${COMPIL_DEFINITION}
)

target_link_libraries(${PROJECT_NAME}.elf o1heap libcanard cyphal-idl)

if(ipo_supported)
    set_property(TARGET ${PROJECT_NAME}.elf PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
endif()

target_compile_options(${PROJECT_NAME}.elf PUBLIC
    -ffile-prefix-map=${CMAKE_SOURCE_DIR}=.
    -grecord-gcc-switches
    ${COMPIL_OPTION}
)

# LOG_LEVEL
target_compile_definitions(${PROJECT_NAME}.elf PUBLIC LOG_LEVEL=${LOG_LEVEL})

# printf configuration
target_compile_definitions(${PROJECT_NAME}.elf PUBLIC
    PRINTF_NTOA_BUFFER_SIZE=32
    PRINTF_FTOA_BUFFER_SIZE=32
    PRINTF_DEFAULT_FLOAT_PRECISION=6
    PRINTF_MAX_FLOAT=1e9
    PRINTF_DISABLE_SUPPORT_EXPONENTIAL=1
    PRINTF_DISABLE_SUPPORT_PTRDIFF_T=1
)

# RTT logs config
target_compile_definitions(${PROJECT_NAME}.elf PUBLIC
  BUFFER_SIZE_UP=8192
)


set(HEX_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.hex)
set(BIN_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.bin)

add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}
    COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}
    COMMENT "Building ${HEX_FILE}
Building ${BIN_FILE}")

find_program(JLINK_EXE JLinkExe REQUIRED)
find_program(JLINK_RTT_CLIENT_EXE JLinkRTTClientExe REQUIRED)
message(STATUS "Found JLink ${JLINK_EXE}")
message(STATUS "Found JLinkRttClient ${JLINK_RTT_CLIENT_EXE}")

# JLink settings
set(JLINK_DEVICE STM32F446ZE)
set(JLINK_SPEED 100000)
set(JLINK_START_ADDRESS 08000000)
set(JLINK_ARGS "")
if (NOT JLINK_SERIAL_NUMBER STREQUAL "")
    list(APPEND JLINK_ARGS "-USB" ${JLINK_SERIAL_NUMBER})
endif()

set(RTT_ARGS "")

if (NOT JLINK_SERIAL_NUMBER STREQUAL "")
    list(APPEND JLINK_ARGS "-USB" ${JLINK_SERIAL_NUMBER})
    list(APPEND RTT_ARGS "--serial-no" ${JLINK_SERIAL_NUMBER})
endif ()
if (NOT REMOTE_FLASH_ADDRESS STREQUAL "")
    list(APPEND JLINK_ARGS "-IP" ${REMOTE_FLASH_ADDRESS})
    list(APPEND RTT_ARGS "--ip-addr" ${REMOTE_FLASH_ADDRESS})
endif ()

configure_file(${CMAKE_SOURCE_DIR}/flash.jlink.in
    ${GENERATED_DIR}/flash.jlink
)

configure_file(${CMAKE_SOURCE_DIR}/halt.jlink.in
    ${GENERATED_DIR}/halt.jlink
)

configure_file(${CMAKE_SOURCE_DIR}/reset.jlink.in
    ${GENERATED_DIR}/reset.jlink
)

configure_file(${CMAKE_SOURCE_DIR}/erase.jlink.in
    ${GENERATED_DIR}/erase.jlink
)

add_custom_target(halt
    DEPENDS ${GENERATED_DIR}/halt.jlink
    COMMAND ${JLINK_EXE} ${JLINK_ARGS} ${GENERATED_DIR}/halt.jlink
)

add_custom_target(reset
    DEPENDS ${GENERATED_DIR}/reset.jlink
    COMMAND ${JLINK_EXE} ${JLINK_ARGS} ${GENERATED_DIR}/reset.jlink
)

add_custom_target(erase
    DEPENDS ${GENERATED_DIR}/erase.jlink
    COMMAND ${JLINK_EXE} ${JLINK_ARGS} ${GENERATED_DIR}/erase.jlink
)

add_custom_target(flash
    DEPENDS ${PROJECT_NAME}.elf
    DEPENDS ${GENERATED_DIR}/flash.jlink
    COMMAND ${JLINK_EXE} ${JLINK_ARGS} ${GENERATED_DIR}/flash.jlink
)

add_custom_target(rtt
    COMMAND ${PYTHON_VENV_BIN_PATH}/rtt ${JLINK_DEVICE} ${RTT_ARGS}
    USES_TERMINAL
)
