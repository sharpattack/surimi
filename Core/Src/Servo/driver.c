#include "Servo/driver.h"
#include "logs.h"
#include "main.h"
#include "tim.h"
#include <cmsis_os2.h>
#include <projdefs.h>
#include <stdint.h>
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_def.h>
#include <stm32f4xx_hal_tim.h>
/*
    3ms period -> 333.333Hz

    TIM1: 180Mhz
    TIM3: 90Mhz
    TIM4: 90Mhz
    TIM5: 90Mhz

    Duty cycle (%) = 100 * (pulse / ARR)
    pulse = (Duty Cycle * ARR) / 100
*/
#define DUTY_CYCLE_TO_PULSE(timer, value) (uint32_t)(value * (timer)->Instance->ARR) / 100
#define WAIT_INTERRUPT_START()                                                                                         \
    while (is_busy == pdTRUE)                                                                                          \
    {                                                                                                                  \
        asm("nop");                                                                                                    \
    }                                                                                                                  \
    __disable_irq();
#define WAIT_INTERRUPT_STOP() __enable_irq();

enum CommandStatus
{
    READY,
    BUSY,
    FINISHED
};

struct ServoCommand
{
    uint8_t duty_cycle;
    uint32_t time;
    enum CommandStatus status;
};

struct Servo
{
    TIM_HandleTypeDef *timer;
    uint32_t channel;
    struct ServoCommand command;
};

struct Servos
{
    uint8_t size;
    struct Servo list[];
};

volatile uint8_t is_busy = pdFALSE;
static uint8_t is_init = pdFALSE;
static struct Servos motors = {.size = 15,
                               .list = {
                                   /* Floor 1 */
                                   {&htim1, TIM_CHANNEL_1, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim1, TIM_CHANNEL_2, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim1, TIM_CHANNEL_3, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim1, TIM_CHANNEL_4, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   /* Floor 2 */
                                   {&htim3, TIM_CHANNEL_1, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim3, TIM_CHANNEL_2, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim3, TIM_CHANNEL_3, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim3, TIM_CHANNEL_4, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   /* Floor 3 */
                                   {&htim4, TIM_CHANNEL_1, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim4, TIM_CHANNEL_2, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim4, TIM_CHANNEL_3, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim4, TIM_CHANNEL_4, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   /* Solar panel */
                                   {&htim5, TIM_CHANNEL_2, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim5, TIM_CHANNEL_3, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                                   {&htim5, TIM_CHANNEL_4, {.duty_cycle = 0, .time = 0, .status = FINISHED}},
                               }};

void Servo_Driver_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance != TIM6)
    {
        return;
    }
    is_busy = pdTRUE;
    for (uint8_t i = 0; i < motors.size; i++)
    {
        struct Servo *motor = &motors.list[i];
        uint8_t duty_cycle = motor->command.duty_cycle;
        if (motor->command.status == FINISHED)
        {
            continue;
        }
        motor->command.status = BUSY;
        if (motor->command.time == 0)
        {
            duty_cycle = 0;
            motor->command.status = FINISHED;
        }
        else
        {
            motor->command.time--;
        }
        __HAL_TIM_SetCompare(motor->timer, motor->channel, DUTY_CYCLE_TO_PULSE(motor->timer, duty_cycle));
    }
    is_busy = pdFALSE;
}

struct Servo *get_servo(ServoHandle_t handle)
{
    if (is_init == pdFALSE)
    {
        LOG_ERROR("Trying to use the servo library but it's not initialized");
        return NULL;
    }
    if (handle >= motors.size)
    {
        LOG_ERROR("Trying to access motor %d, value range is 0-%d", handle, motors.size - 1);
        return NULL;
    }
    return &motors.list[handle];
}

HAL_StatusTypeDef Servo_wait(ServoHandle_t handle, uint32_t max_wait)
{
    struct Servo *motor = get_servo(handle);
    uint32_t start = HAL_GetTick();
    if (motor == NULL)
    {
        return HAL_ERROR;
    }
    while (motor->command.status != FINISHED)
    {
        if (HAL_GetTick() - start > max_wait)
        {
            return HAL_TIMEOUT;
        }
        sleep(1);
    }
    return HAL_OK;
}

HAL_StatusTypeDef Servo_set_duty_cycle(ServoHandle_t handle, uint8_t duty_cycle, uint32_t time, uint32_t max_wait)
{
    struct Servo *motor = get_servo(handle);
    if (motor == NULL)
    {
        return HAL_ERROR;
    }
    if (duty_cycle > 100)
    {
        LOG_WARNING("Servo set to duty cycle %d, max is 100", duty_cycle);
        return HAL_ERROR;
    }

    WAIT_INTERRUPT_START();
    motor->command.duty_cycle = duty_cycle;
    motor->command.time = time;
    motor->command.status = READY;
    WAIT_INTERRUPT_STOP();

    return Servo_wait(handle, max_wait);
}

HAL_StatusTypeDef Servo_init()
{
    uint8_t deinit = 0;
    HAL_StatusTypeDef status = HAL_ERROR;

    if (is_init == pdTRUE)
    {
        return HAL_OK;
    }

    if ((status = HAL_TIM_Base_Start_IT(&htim6)) != HAL_OK)
    {
        LOG_ERROR("Could not initialize TIM6 with error %d", status);
        return status;
    }

    for (uint8_t i = 0; i < motors.size; i++)
    {
        struct Servo *motor = &motors.list[i];
        LOG_DEBUG("Initializing servo %d", i);
        if ((status = HAL_TIM_PWM_Start(motor->timer, motor->channel)) != HAL_OK)
        {
            goto error;
        }
        deinit++;
    }

    is_init = pdTRUE;
    return HAL_OK;
error:
    LOG_ERROR("Servo initialization error at motor %d with status %d", deinit, status);
    for (uint8_t i = 0; i < deinit; i++)
    {
        LOG_ERROR("Stopping PWM %d", i);
        struct Servo *motor = &motors.list[i];
        HAL_TIM_PWM_Stop(motor->timer, motor->channel);
    }
    return status;
}
