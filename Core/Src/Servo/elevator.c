#include "Servo/elevator.h"
#include "FreeRTOSConfig.h"
#include "driver.h"
#include "logs.h"
#include <portmacro.h>
#include <projdefs.h>
#include <stdbool.h>
#include <stdint.h>
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_def.h>

static uint8_t is_init = pdFALSE;
extern bool motor_homing_finished;
bool claws_retracted = false;

#define SOLAR_PANEL_UPRIGHT 55
#define SOLAR_PANEL_DOWRIGHT 70

uint8_t Servo_Elevator_is_initialized()
{
    return is_init;
}

void _retract_solar()
{
    Servo_set_duty_cycle(SOLAR_PANEL_1, SOLAR_PANEL_UPRIGHT, portMAX_DELAY, 0);
}

void _extend_solar()
{

    Servo_set_duty_cycle(SOLAR_PANEL_1, SOLAR_PANEL_DOWRIGHT, portMAX_DELAY, 0);
}

void _turn_solar(uint8_t start, uint8_t end)
{

    Servo_set_duty_cycle(SOLAR_PANEL_2, start, portMAX_DELAY, 0);
    sleep(1000);
    _extend_solar();
    sleep(500);
    Servo_set_duty_cycle(SOLAR_PANEL_2, end, portMAX_DELAY, 0);
    sleep(1000);
    _retract_solar();
}

HAL_StatusTypeDef _use(Type type, Floor floor, Side side, Direction direction, uint32_t max_wait)
{
    return Servo_set_duty_cycle(
        type | floor | side, direction,
        MAX_ON_OFF_DELAY - 2 /* We want to make sure we never get a timeout when using the constant in the user api */,
        max_wait);
}

HAL_StatusTypeDef _close_claw(Floor floor, Side side, uint32_t max_wait)
{
    /* See with Robin which side to turn */
    return _use(CLAW, floor, side, CLOCKWISE, max_wait);
}

HAL_StatusTypeDef _open_claw(Floor floor, Side side, uint32_t max_wait)
{
    /* See with Robin which side to turn */
    return _use(CLAW, floor, side, ANTICLOCKWISE, max_wait);
}

HAL_StatusTypeDef _extend_rack(Floor floor, Side side, uint32_t max_wait)
{
    Direction direction = ANTICLOCKWISE;
    if (side == RIGHT)
    {
        direction = CLOCKWISE;
    }
    return _use(RACK, floor, side, direction, max_wait);
}

HAL_StatusTypeDef _retract_rack(Floor floor, Side side, uint32_t max_wait)
{
    Direction direction = CLOCKWISE;
    if (side == RIGHT)
    {
        direction = ANTICLOCKWISE;
    }
    return _use(RACK, floor, side, direction, max_wait);
}

HAL_StatusTypeDef Servo_Elevator_init()
{
#define HANDLE_ERROR(expr)                                                                                             \
    if ((status = expr) != HAL_OK)                                                                                     \
    {                                                                                                                  \
        return status;                                                                                                 \
    }
    HAL_StatusTypeDef status = HAL_ERROR;

    /* Don't initialize the library twice */
    if (is_init == pdTRUE)
    {
        return pdTRUE;
    }

    /* Ensure that the servo lib is enabled */
    if ((status = Servo_init()) != HAL_OK)
    {
        return status;
    }

    _retract_solar();

    /* Close all claws and extend everything */
    _close_claw(FLOOR_1, LEFT, 0);
    _close_claw(FLOOR_1, RIGHT, 0);
    _close_claw(FLOOR_2, LEFT, 0);
    _close_claw(FLOOR_2, RIGHT, MAX_ON_OFF_DELAY);

    _extend_rack(FLOOR_1, LEFT, 0);
    _extend_rack(FLOOR_1, RIGHT, 0);
    _extend_rack(FLOOR_2, RIGHT, 0);
    _extend_rack(FLOOR_2, LEFT, MAX_ON_OFF_DELAY);

    claws_retracted = true;

    while (!motor_homing_finished)
    {
        sleep(1);
    }

    _retract_rack(FLOOR_1, LEFT, 0);
    _retract_rack(FLOOR_1, RIGHT, 0);
    _retract_rack(FLOOR_2, RIGHT, 0);
    _retract_rack(FLOOR_2, LEFT, MAX_ON_OFF_DELAY);

    is_init = pdTRUE;
    return HAL_OK;
}

#define API_TEMPLATE(low_level_mapping, ...)                                                                           \
    if (is_init != pdTRUE)                                                                                             \
    {                                                                                                                  \
        return HAL_ERROR;                                                                                              \
    }                                                                                                                  \
    return low_level_mapping(__VA_ARGS__);

HAL_StatusTypeDef Servo_Elevator_retract(Floor floor, Side side, uint32_t max_wait)
{
    API_TEMPLATE(_retract_rack, floor, side, max_wait);
}
HAL_StatusTypeDef Servo_Elevator_extend(Floor floor, Side side, uint32_t max_wait)
{
    API_TEMPLATE(_extend_rack, floor, side, max_wait);
}

HAL_StatusTypeDef Servo_Elevator_open(Floor floor, Side side, uint32_t max_wait)
{
    API_TEMPLATE(_open_claw, floor, side, max_wait);
}
HAL_StatusTypeDef Servo_Elevator_close(Floor floor, Side side, uint32_t max_wait)
{
    API_TEMPLATE(_close_claw, floor, side, max_wait);
}

void Servo_Elevator_Solar_turn_yellow()
{
    _turn_solar(1, 99);
}

void Servo_Elevator_Solar_turn_blue()
{
    _turn_solar(99, 1);
}
