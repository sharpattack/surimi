/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2024 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "main.h"
#include "task.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Tasks/task_canard.h"
#include "Tasks/task_led.h"
#include "Tasks/task_servo_ctrl.h"
#include "Tasks/task_step_motor.h"
#include "logs.h"
#include <cmsis_os2.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define STRINGIFY(x) #x
#define TASK_HELPER(task_name, buffer_size, task_priority)                                                             \
    osThreadId_t task_name##TaskHandle;                                                                                \
    uint32_t task_name##TaskBuffer[buffer_size];                                                                       \
    osStaticThreadDef_t task_name##TaskControlBlock;                                                                   \
    const osThreadAttr_t task_name##Task_attributes = {                                                                \
        .name = STRINGIFY(task_name##Task),                                                                            \
        .cb_mem = &task_name##TaskControlBlock,                                                                        \
        .cb_size = sizeof(task_name##TaskControlBlock),                                                                \
        .stack_mem = &task_name##TaskBuffer[0],                                                                        \
        .stack_size = sizeof(task_name##TaskBuffer),                                                                   \
        .priority = (osPriority_t)(task_priority),                                                                     \
    }

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
const struct vLEDTaskParameters LEDTaskParameters = {.gpio_port = LED_HEARTBEAT_GPIO_Port,
                                                     .gpio_pin = LED_HEARTBEAT_Pin};

/* Definition of servo task */
TASK_HELPER(ServoCtrl, 1024, osPriorityRealtime);

/* Definition of Step Motor task */
TASK_HELPER(StepMotor, 1024, osPriorityRealtime);

/* Definition of canard task */
TASK_HELPER(Canard, 1024, osPriorityNormal);

/* USER CODE END Variables */
/* Definitions for LedTask */
osThreadId_t LedTaskHandle;
uint32_t LedTaskBuffer[128];
osStaticThreadDef_t LedTaskControlBlock;
const osThreadAttr_t LedTask_attributes = {
    .name = "LedTask",
    .cb_mem = &LedTaskControlBlock,
    .cb_size = sizeof(LedTaskControlBlock),
    .stack_mem = &LedTaskBuffer[0],
    .stack_size = sizeof(LedTaskBuffer),
    .priority = (osPriority_t)osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartLedTask(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
    /* Run time stack overflow checking is performed if
    configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
    called if a stack overflow is detected. */
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void)
{
    /* vApplicationMallocFailedHook() will only be called if
    configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
    function that will get called if a call to pvPortMalloc() fails.
    pvPortMalloc() is called internally by the kernel whenever a task, queue,
    timer or semaphore is created. It is also called by various parts of the
    demo application. If heap_1.c or heap_2.c are used, then the size of the
    heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
    FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
    to query the size of free heap space that remains (although it does not
    provide information on how the remaining heap might be fragmented). */
}
/* USER CODE END 5 */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void)
{
    /* USER CODE BEGIN Init */
    SEGGER_SYSVIEW_Conf();
    init_logs();

    /* USER CODE END Init */

    /* USER CODE BEGIN RTOS_MUTEX */
    /* add mutexes, ... */
    /* USER CODE END RTOS_MUTEX */

    /* USER CODE BEGIN RTOS_SEMAPHORES */
    /* add semaphores, ... */
    /* USER CODE END RTOS_SEMAPHORES */

    /* USER CODE BEGIN RTOS_TIMERS */
    /* start timers, add new ones, ... */
    /* USER CODE END RTOS_TIMERS */

    /* USER CODE BEGIN RTOS_QUEUES */
    /* add queues, ... */
    /* USER CODE END RTOS_QUEUES */

    /* Create the thread(s) */
    /* creation of LedTask */
    LedTaskHandle = osThreadNew(StartLedTask, (void *)&LEDTaskParameters, &LedTask_attributes);

    /* USER CODE BEGIN RTOS_THREADS */

    CanardTaskHandle = osThreadNew(StartCanardTask, NULL, &CanardTask_attributes);
    ServoCtrlTaskHandle = osThreadNew(StartServoCtrlTask, NULL, &ServoCtrlTask_attributes);
    StepMotorTaskHandle = osThreadNew(StartStepMotorTask, NULL, &StepMotorTask_attributes);

    /* USER CODE END RTOS_THREADS */

    /* USER CODE BEGIN RTOS_EVENTS */
    /* add events, ... */
    /* USER CODE END RTOS_EVENTS */
}

/* USER CODE BEGIN Header_StartLedTask */
/**
 * @brief  Function implementing the LedTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartLedTask */
void StartLedTask(void *argument)
{
    /* USER CODE BEGIN StartLedTask */
    vStartLedTask(argument);
    /* Infinite loop */
    for (;;)
    {
        osDelay(1);
    }
    /* USER CODE END StartLedTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */
