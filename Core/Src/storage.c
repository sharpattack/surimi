#include "storage.h"
#include "logs.h"
#include <embd_string.h>
#include <stdbool.h>
#include <stdint.h>

#define RAM_INITIALIZED 0xCACACACA

__attribute__((section(".no_init"))) uint32_t initialized;
__attribute__((section(".no_init"))) DataStore storage;

void reset_data_store()
{
    initialized = RAM_INITIALIZED;
    storage.nodeId = 0xFF;
    storage.baudrate = 500000u;
    storage.counter = 0;
    memset(storage.nvm_memory, 0xffu, NVM_SIM_SIZE);
}

DataStore *get_data_store()
{

    /* Test that RAM is uninitialized */
    if (initialized == RAM_INITIALIZED)
    {
        return &storage;
    }

    /* If this doesn't match, we initialize with default values */
    reset_data_store();

    return &storage;
}
