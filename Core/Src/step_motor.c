#include "step_motor.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "TMC5130.h"
#include "TMC5130_Fields.h"
#include "Types.h"
#include "cmsis_os.h"
#include "event_groups.h"
#include "logs.h"
#include "main.h"
#include "motors.h"
#include "portmacro.h"
#include "projdefs.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_def.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc_ex.h"
#include "stm32f4xx_hal_tim.h"
#include "tim.h"
#include <math.h>
#include <stdbool.h>
#include <stdint.h>

/*  _____      _            _                  _____ _____  */
/* |  __ \    (_)          | |           /\   |  __ \_   _| */
/* | |__) | __ ___   ____ _| |_ ___     /  \  | |__) || |   */
/* |  ___/ '__| \ \ / / _` | __/ _ \   / /\ \ |  ___/ | |   */
/* | |   | |  | |\ V / (_| | ||  __/  / ____ \| |    _| |_  */
/* |_|   |_|  |_| \_/ \__,_|\__\___| /_/    \_\_|   |_____| */
/*                                                          */

#define STEPPING 256
uint8_t motor_nb = 0;

typedef struct
{
    bool initialized; /* Mark if the motor is used or not */

    /* Wiring config */
    StepMotorChannel channel;
    TMC5130TypeDef handle;
    ConfigurationTypeDef config;

    /* Motor specification */
    uint32_t steps_for_one_turn; /* Number of step for a full turn with no micro-stepping */
} StepMotor_cfg;

static StepMotor_cfg motors[MAX_STEPPER_MOTORS]; /* Array of stepper motors to control with the library */

/**
 * Get the maximum number of steps for a motor taking stepping into account
 *
 * @param motor pointer to the motor config
 *
 * @return number of steps for a turn
 **/
uint32_t _max_motor_steps(StepMotor_cfg *motor)
{
    return motor->steps_for_one_turn * STEPPING;
}

/**
 * Convert steps to rad for a motor
 *
 * @param motor pointer to the motor config
 * @param steps number of steps to convert
 *
 * @return angle in rad
 **/
float32_t _step_to_rad(StepMotor_cfg *motor, int32_t steps)
{
    return steps * (2.0f * M_PI) / _max_motor_steps(motor);
}

/**
 * Convert rad to steps for a motor
 *
 * @param motor pointer to the motor config
 * @param rad angle to convert
 *
 * @return steps as an integer
 **/
int32_t _rad_to_step(StepMotor_cfg *motor, float32_t rad)
{
    return (int32_t)(rad * _max_motor_steps(motor) / (2.0f * M_PI));
}

/**
 * Get the curent position of the motor in rad/s
 *
 * @param motor pointer to the motor config
 *
 * @return number of steps for a turn
 **/
float32_t get_absolute_position(StepMotor_cfg *motor)
{
    tmc5130_readInt(&motor->handle, TMC5130_XACTUAL); /* You have to read twice to get an updated value with SPI */
    return _step_to_rad(motor, tmc5130_readInt(&motor->handle, TMC5130_XACTUAL));
}

/**
 * Get maximum motor speed in rad/s
 *
 * @param motor pointer to the motor config
 *
 * @return speed in rad/s
 **/
float32_t _max_motor_speed(StepMotor_cfg *motor)
{
    // Max VMAX value is (2^23)-512 [μsteps / t] = 8388096 [μsteps / t]
    return 8388096 * ((2 * M_PI) / _max_motor_steps(motor));
}

/**
 * Check the RAMPSTAT register events if the motor reached expected position
 *
 * @param motor pointer to the motor config
 *
 * @return if the motor reached destination
 **/
bool _has_reached_expected_position(StepMotor_cfg *motor)
{
    return TMC5130_FIELD_READ(&motor->handle, TMC5130_RAMPSTAT, TMC5130_POSITION_REACHED_MASK,
                              TMC5130_POSITION_REACHED_SHIFT);
    // return (tmc5130_readInt(&motor->handle, TMC5130_RAMPSTAT) >> TMC5130_POSITION_REACHED_SHIFT) & 0x1;
}

/**
 * Wait for the motor to reach it's destination
 *
 * @param motor pointer to the motor config
 * @param timeout, maximum time to wait for that motor
 *
 * @return success status
 **/
HAL_StatusTypeDef _wait_motor(StepMotor_cfg *motor, TickType_t timeout)
{
    TickType_t start_time = HAL_GetTick();
    while (!_has_reached_expected_position(motor))
    {
        if (HAL_GetTick() - start_time > timeout)
        {
            LOG_ERROR("Call to motor %hhu timed out with wait time %lu", motor->config.configIndex, timeout);
            return HAL_ERROR;
        }
        sleep(1);
    }
    return HAL_OK;
}

/**
 * Turn the motor with a specified absolute step position
 *
 * @param motor   pointer to the motor config
 * @param steps   steps to go
 * @param speed   steps per seconds
 *
 * @return success status
 **/
HAL_StatusTypeDef _absolute_step(StepMotor_cfg *motor, int32_t absolute_step_position, uint32_t speed)
{
    LOG_DEBUG("Stepping motor %hhu with %ld steps at %lu ustep/s", motor->config.configIndex, absolute_step_position,
              speed);
    _has_reached_expected_position(motor); // clear RAMPSTAT
    tmc5130_writeInt(&motor->handle, TMC5130_VMAX, (int32_t)(speed));
    tmc5130_writeInt(&motor->handle, TMC5130_XTARGET, absolute_step_position);
    _has_reached_expected_position(motor); // clear RAMPSTAT
    return HAL_OK;
}

/**
 * Turn the motor with a specified angle relative to it's current position
 *
 * @param motor   pointer to the motor config
 * @param steps   angle to turn in rad
 * @param speed   speed of the motor in rad/s
 *
 * @return success status
 **/
HAL_StatusTypeDef _absolute_angle(StepMotor_cfg *motor, float32_t angle, float32_t speed)
{
    LOG_DEBUG("Stepping motor %hhu with angle %f at %f rad/s", motor->config.configIndex, angle, speed);

    return _absolute_step(motor, _rad_to_step(motor, angle), (speed * _max_motor_steps(motor)) / (2.0 * M_PI));
}

/**
 * Get the first available uninitialized motor slot
 *
 * @return uninitialized motor or -1 if could not find one
 **/
int8_t _get_uninitialized_motor_slot()
{
    for (int8_t i = 0; i < MAX_STEPPER_MOTORS; ++i)
    {
        if (!motors[i].initialized)
            return i;
    }
    return -1;
}

/**
 * Get a motor config pointer from a motor id
 *
 * @param motor descriptor
 *
 * @return pointer to the motor config, NULL if invalid
 **/
StepMotor_cfg *_get_motor_cfg(StepMotor motor)
{
    if (motor.index < 0 || motor.index > MAX_STEPPER_MOTORS)
        return NULL;
    StepMotor_cfg *cfg = (StepMotor_cfg *)&motors[motor.index];
    if (!cfg->initialized)
        return NULL;
    return cfg;
}

/*  _____       _     _ _                 _____ _____  */
/* |  __ \     | |   | (_)          /\   |  __ \_   _| */
/* | |__) |   _| |__ | |_  ___     /  \  | |__) || |   */
/* |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |   */
/* | |   | |_| | |_) | | | (__   / ____ \| |    _| |_  */
/* |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____| */
/*                                                     */

HAL_StatusTypeDef StepMotor_init()
{

    /* Set all motors as uninitialized */
    for (uint8_t i = 0; i < MAX_STEPPER_MOTORS; ++i)
        motors[i].initialized = false;

    return HAL_OK;
}

void StepMotor_deinit()
{
    /* Disable event handle */
    /* Disable all motors */
    for (uint8_t i = 0; i < MAX_STEPPER_MOTORS; ++i)
    {
        if (motors[i].initialized)
        {
            tmc5130_writeInt(&motors[i].handle, TMC5130_VMAX, 0);
        }
        motors[i].initialized = false;
    }
}

StepMotor StepMotor_register(GPIO_TypeDef *gpio_channel, uint16_t pin_channel, uint32_t steps_for_one_turn)
{
    StepMotor step_motor = {.index = _get_uninitialized_motor_slot(), .handle = NULL};
    if (step_motor.index < 0)
        return step_motor;

    StepMotor_cfg *cfg = &motors[step_motor.index];
    step_motor.handle = &cfg->handle;

    /* Configure motor */
    cfg->channel.gpio = gpio_channel;
    cfg->channel.pin = pin_channel;

    step_motor_init(&cfg->handle, cfg->channel, &cfg->config);
    cfg->steps_for_one_turn = steps_for_one_turn;

    tmc5130_reset(&cfg->handle);

    // Config qui fonctionne !!!
    tmc5130_writeInt(&cfg->handle, TMC5130_GCONF, 4); // Enable stealthCHop (en_pwm_mode)
    // tmc5130_writeInt(&motor->handle, TMC5130_GCONF,      0xB);  // Enable stealthCHop (en_pwm_mode) +
    // (enc_commutation)
    tmc5130_writeInt(&cfg->handle, TMC5130_TPWMTHRS, 500);
    tmc5130_writeInt(&cfg->handle, TMC5130_PWMCONF, 0x000401C8);

    tmc5130_writeInt(&cfg->handle, TMC5130_CHOPCONF, 0x000100C3);

    tmc5130_writeDatagram(&cfg->handle, TMC5130_IHOLD_IRUN,
                          0,  // Ignored
                          7,  // IHOLDDELAY
                          16, // IRUN
                          7   // IHOLD
    );                        // 0.00Apeak  / 0.71ARMS

    // Reset position
    tmc5130_writeInt(&cfg->handle, TMC5130_RAMPMODE, TMC5130_MODE_POSITION);
    tmc5130_writeInt(&cfg->handle, TMC5130_XTARGET, 0);
    tmc5130_writeInt(&cfg->handle, TMC5130_XACTUAL, 0);

    // Standard values for speed and acceleration
    tmc5130_writeInt(&cfg->handle, TMC5130_VSTART, 1);
    tmc5130_writeInt(&cfg->handle, TMC5130_A1, 30000);
    tmc5130_writeInt(&cfg->handle, TMC5130_V1, 50000);
    tmc5130_writeInt(&cfg->handle, TMC5130_AMAX, 10000);
    tmc5130_writeInt(&cfg->handle, TMC5130_VMAX, 200000);
    tmc5130_writeInt(&cfg->handle, TMC5130_DMAX, 10000);
    tmc5130_writeInt(&cfg->handle, TMC5130_D1, 30000);
    tmc5130_writeInt(&cfg->handle, TMC5130_VSTOP, 10);

    tmc5130_writeInt(&cfg->handle, TMC5130_SWMODE, 0x0);

    /* Enable motor */
    cfg->initialized = true;

    return step_motor;
}

HAL_StatusTypeDef StepMotor_homing(StepMotor motor, bool forward, bool sensor_left, bool sensor_right,
                                   float32_t initial_angle)
{
    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return HAL_ERROR;

    int8_t direction = 1;
    if (!forward)
        direction = -1;

    int32_t sensor_config = 0x00000C; // strokendsensors active low
    // int32_t sensor_config = 0x000000; //strokendsensors active high
    sensor_config |= (sensor_right << 1);
    sensor_config |= sensor_left;

    // configure latching
    sensor_config |= (sensor_right << 7);
    sensor_config |= (sensor_left << 5);

    tmc5130_writeInt(&cfg->handle, TMC5130_SWMODE, sensor_config);

    if (forward)
    {
        tmc5130_writeInt(&cfg->handle, TMC5130_RAMPMODE, TMC5130_MODE_VELPOS);
    }
    else
    {
        tmc5130_writeInt(&cfg->handle, TMC5130_RAMPMODE, TMC5130_MODE_VELNEG);
    }
    // 255999 is a nice speed
    tmc5130_writeInt(&cfg->handle, TMC5130_VMAX, (int32_t)(255999));

    /* We can check left and right at the same time, the driver only trigger the relevant one anyway */
    tmc5130_readInt(motor.handle, TMC5130_RAMPSTAT); /* Clear flags */
    uint32_t rampstat = tmc5130_readInt(motor.handle, TMC5130_RAMPSTAT);
    while (!FIELD_GET(rampstat, TMC5130_EVENT_STOP_L_MASK, TMC5130_EVENT_STOP_L_SHIFT) &&
           !FIELD_GET(rampstat, TMC5130_EVENT_STOP_R_MASK, TMC5130_EVENT_STOP_R_SHIFT))
    {
        rampstat = tmc5130_readInt(motor.handle, TMC5130_RAMPSTAT);
        sleep(1);
    }

    tmc5130_writeInt(&cfg->handle, TMC5130_VMAX, 0);
    tmc5130_writeInt(&cfg->handle, TMC5130_RAMPMODE, TMC5130_MODE_HOLD);
    tmc5130_writeInt(&cfg->handle, TMC5130_XACTUAL, _rad_to_step(cfg, initial_angle));
    tmc5130_writeInt(&cfg->handle, TMC5130_XTARGET, _rad_to_step(cfg, initial_angle));
    tmc5130_writeInt(&cfg->handle, TMC5130_RAMPMODE, TMC5130_MODE_POSITION);
    return HAL_OK;
}

float32_t StepMotor_get_current_angle(StepMotor motor)
{
    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return 0.0;
    return get_absolute_position(cfg);
}

float32_t StepMotor_max_speed(StepMotor motor)
{
    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return 0.0;
    return _max_motor_speed(cfg);
}

float32_t StepMotor_step_speed_to_angle(StepMotor motor, uint32_t speed)
{
    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return 0.0;

    return (speed * 2.0 * M_PI) / (cfg->steps_for_one_turn * STEPPING * 1.0);
}

HAL_StatusTypeDef StepMotor_angle(StepMotor motor, float32_t angle, float32_t speed, TickType_t timeout)
{
    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return HAL_ERROR;
    if (speed <= 0 || speed > _max_motor_speed(cfg))
        return HAL_ERROR;

    _absolute_angle(cfg, angle, speed);
    return _wait_motor(cfg, timeout);
}

HAL_StatusTypeDef StepMotor_angle_async(StepMotor motor, float32_t angle, float32_t speed)
{
    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return 0;
    if (speed <= 0 || speed > _max_motor_speed(cfg))
        return HAL_ERROR;

    return _absolute_angle(cfg, angle, speed);
}

HAL_StatusTypeDef StepMotor_wait(StepMotor motor, TickType_t timeout)
{

    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return HAL_ERROR;

    return _wait_motor(cfg, timeout);
}

HAL_StatusTypeDef _StepMotor_wait_n(StepMotor *motors, TickType_t timeout)
{
    size_t i = 0;
    while (motors[i].handle != NULL)
    {
        if (StepMotor_wait(motors[i], timeout) != HAL_OK)
        {
            return HAL_ERROR;
        }
        i++;
    }
    return HAL_OK;
}

bool StepMotor_finished(StepMotor motor)
{

    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return false;

    return _has_reached_expected_position(cfg);
}

void StepMotor_unregister(StepMotor motor)
{
    StepMotor_cfg *cfg = _get_motor_cfg(motor);
    if (cfg == NULL)
        return;
    cfg->initialized = false;
}
