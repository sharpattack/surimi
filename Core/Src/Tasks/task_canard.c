#include "Tasks/task_canard.h"
#include "canard.h"
#include "logs.h"
#include "opencyphal/uavcan/node/Heartbeat_1_0.h"
#include "opencyphal/uavcan/sharp/network/Reboot_1_0.h"
#include "uavcan/sharp/network/Ready_1_0.h"

#include "can.h"
#include "o1heap.h"
#include "portmacro.h"
#include "projdefs.h"
#include "semphr.h"
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_can.h>

#define UAVCAN_MEM_SIZE 1024
#define CANARD_RX_QUEUE_SIZE 100
#define CANARD_TX_QUEUE_SIZE 100

/* Canard and o1heap ARE NOT THREAD SAFE !! That's why we have mutexes */
SemaphoreHandle_t canard_mutex = NULL;
StaticSemaphore_t canard_mutex_buffer;

O1HeapInstance *allocator = NULL;

CanardInstance
    canard; // This is the core structure that keeps all of the states and allocated resources of the library instance
CanardTxQueue canard_tx_queue; // Prioritized transmission queue that keeps CAN frames destined for transmission via one
                               // CAN interface

typedef struct
{
    CAN_RxHeaderTypeDef header;
    uint8_t data[8];
} CAN_RxFrame;

QueueHandle_t canard_rx_queue = NULL;
static StaticQueue_t canard_rx_queue_buffer;
uint8_t canard_rx_queue_storage[CANARD_RX_QUEUE_SIZE * sizeof(CAN_RxFrame)];

CanardMicrosecond micros()
{
    return HAL_GetTick() * 1000; /* It's a hack but that's good enough */
}

int32_t _publish_canard_payload(uint8_t *payload, size_t payload_size, CanardTransferMetadata *metadata)
{
    int32_t err;
    while (canard_mutex == NULL)
    {
        LOG_INFO("Waiting for canard to initialize");
        sleep(10);
    }
    xSemaphoreTake(canard_mutex, portMAX_DELAY);
    err = canardTxPush(&canard_tx_queue, &canard, 0, /* Zero if transmission deadline is not limited. */
                       metadata, payload_size, payload);
    xSemaphoreGive(canard_mutex);
    return err;
}

int8_t _subsrcibe_canard_topic(const CanardPortID port_id, const size_t extent,
                               CanardRxSubscription *const out_subscription)
{
    uint8_t err;
    while (canard_mutex == NULL)
    {
        LOG_INFO("Waiting for canard to initialize");
        sleep(10);
    }
    xSemaphoreTake(canard_mutex, portMAX_DELAY);
    // subsrcibe with multicast only
    err = canardRxSubscribe((CanardInstance *const)&canard, CanardTransferKindMessage, port_id, extent,
                            CANARD_DEFAULT_TRANSFER_ID_TIMEOUT_USEC, out_subscription);

    xSemaphoreGive(canard_mutex);
    return err;
}

uint8_t memory[UAVCAN_MEM_SIZE] __attribute__((aligned O1HEAP_ALIGNMENT));

static void *o1malloc(CanardInstance *const canard, const size_t amount)
{
    (void)canard;
    return o1heapAllocate(allocator, amount);
}

static void o1free(CanardInstance *const canard, void *const pointer)
{
    (void)canard;
    o1heapFree(allocator, pointer);
}

static void configure_can(CAN_HandleTypeDef *selected_can)
{
    HAL_StatusTypeDef result;

    /* Default filter - accept all to CAN_FIFO*/
    CAN_FilterTypeDef filter = {0};
    filter.FilterBank = 14;
    filter.FilterMode = CAN_FILTERMODE_IDMASK;
    filter.FilterScale = CAN_FILTERSCALE_32BIT;
    filter.FilterIdHigh = 0x0000;
    filter.FilterIdLow = 0x0000;
    filter.FilterMaskIdHigh = 0x0000;
    filter.FilterMaskIdLow = 0x0000;
    filter.FilterFIFOAssignment = CAN_RX_FIFO1;
    filter.FilterActivation = ENABLE;
    filter.SlaveStartFilterBank = 14; /* yes */

    canard_rx_queue =
        xQueueCreateStatic(CANARD_RX_QUEUE_SIZE, sizeof(CAN_RxFrame), canard_rx_queue_storage, &canard_rx_queue_buffer);
    if (canard_rx_queue == NULL)
    {
        LOG_ERROR("Could not initialize canard rx queue");
        for (;;)
        {
        }
    }

    result = HAL_CAN_ConfigFilter(selected_can, &filter);
    if (result != HAL_OK)
    {
        LOG_ERROR("Could not initialize can filters with error %d", result);
        for (;;)
        {
        }
    }

    result = HAL_CAN_Start(selected_can);
    if (result != HAL_OK)
    {
        LOG_ERROR("Could not initialize can with error %d", result);
        for (;;)
        {
        }
    }
    HAL_CAN_ActivateNotification(selected_can, CAN_IT_RX_FIFO1_MSG_PENDING);
}

void process_canard_TX_queue(void)
{
    xSemaphoreTake(canard_mutex, portMAX_DELAY);
    for (const CanardTxQueueItem *ti = NULL; (ti = canardTxPeek(&canard_tx_queue)) != NULL;)
    {
        /* Instantiate a frame for the media layer */
        CAN_TxHeaderTypeDef TxHeader;
        TxHeader.IDE = CAN_ID_EXT;
        TxHeader.RTR = CAN_RTR_DATA;

        TxHeader.DLC = ti->frame.payload_size;
        TxHeader.ExtId = ti->frame.extended_can_id;

        uint8_t TxData[8];
        uint32_t TxMailbox;

        memcpy(TxData, (uint8_t *)ti->frame.payload, ti->frame.payload_size);

        if (HAL_CAN_AddTxMessage(&hcan2, &TxHeader, TxData, &TxMailbox) != HAL_OK)
        {
            break;
        }
        // After the frame is transmitted or if it has timed out while waiting, pop it from the queue and deallocate:
        canard.memory_free(&canard, canardTxPop(&canard_tx_queue, ti));
    }
    xSemaphoreGive(canard_mutex);
}

void process_canard_RX_queue()
{
    CAN_RxFrame frame;
    xSemaphoreTake(canard_mutex, portMAX_DELAY);
    while (xQueueReceive(canard_rx_queue, &frame, 0))
    {
        CanardFrame rxf;

        rxf.extended_can_id = frame.header.ExtId;
        rxf.payload_size = (size_t)frame.header.DLC;
        rxf.payload = (void *)frame.data;

        CanardRxTransfer transfer;
        CanardRxSubscription *subscription;

        if (canardRxAccept((CanardInstance *const)&canard, micros(), &rxf, 0, &transfer, &subscription) != 1)
        {
            continue;
        }
        ((void (*)(CanardRxTransfer *))(subscription->user_reference))(&transfer);
        canard.memory_free(&canard, transfer.payload);
    }
    xSemaphoreGive(canard_mutex);
}

#pragma optimize = s none
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
    (void)hcan;
    BaseType_t priority = pdFALSE;
    if (canard_rx_queue == NULL)
    {
        LOG_WARNING("Receiving CAN messages before canard has finished it's config")
        return;
    }

    CAN_RxFrame frame;
    HAL_CAN_GetRxMessage(&hcan2, CAN_RX_FIFO1, &frame.header, frame.data);
    xQueueSendFromISR(canard_rx_queue, &frame, &priority);

    portYIELD_FROM_ISR(priority);
}

// buffer for serialization of a heartbeat message
CANARD_PREPARE_PUBLISH_FIXED_ID(uavcan_node_Heartbeat_1_0);

#if IS_MASTER_NODE
CANARD_PREPARE_PUBLISH_FIXED_ID(uavcan_sharp_network_Reboot_1_0);
CANARD_PREPARE_SUBSCRIBE_FIXED_ID(uavcan_sharp_network_Ready_1_0, MAX_FEATURES);
#else
CANARD_PREPARE_SUBSCRIBE_FIXED_ID(uavcan_sharp_network_Reboot_1_0, 1);
CANARD_PREPARE_PUBLISH_FIXED_ID(uavcan_sharp_network_Ready_1_0);
#endif

#if IS_MASTER_NODE
void reset_all_cards()
{
    uavcan_sharp_network_Reboot_1_0 command = {.value = true};
    CANARD_PUBLISH_MULTICAST_FIXED_ID(uavcan_sharp_network_Reboot_1_0, CanardPriorityImmediate, command);
}
bool wait_for_feature(uint8_t feature, TickType_t timeout)
{
    TickType_t start_time = HAL_GetTick();
    QueueHandle_t messages = CANARD_GET_QUEUE_FIXED_ID(uavcan_sharp_network_Ready_1_0);
    while (HAL_GetTick() - start_time <= timeout)
    {
        uavcan_sharp_network_Ready_1_0 message = {};

        while (xQueueReceive(messages, &message, 0))
        {
            if (message.feature == feature)
            {
                return HAL_OK;
            }
            xQueueSendToBack(messages, (void *)&message, 0);
        }
        sleep(1);
    }
    return HAL_TIMEOUT;
}
#else
void monitor_network_commands()
{
    QueueHandle_t commands = CANARD_GET_QUEUE_FIXED_ID(uavcan_sharp_network_Reboot_1_0);
    uavcan_sharp_network_Reboot_1_0 command = {};
    while (xQueueReceive(commands, &command, 0))
    {
        if (command.value)
        {
            NVIC_SystemReset();
        }
    }
}
void ready_for_feature(uint8_t feature)
{
    uavcan_sharp_network_Ready_1_0 ready_message = {.feature = feature};
    CANARD_PUBLISH_MULTICAST_FIXED_ID(uavcan_sharp_network_Ready_1_0, CanardPriorityImmediate, ready_message);
}
#endif

CanardRxSubscription subscription;
_Noreturn void StartCanardTask(void *parameters)
{
    (void)parameters;
    configure_can(&hcan2);
    uint32_t serial_number = HAL_GetUIDw0();
    LOG_INFO("SN 0x%2lx | ID 0x%2x", serial_number, (uint8_t)serial_number);

    allocator = o1heapInit(memory, UAVCAN_MEM_SIZE);
    canard = canardInit(&o1malloc, &o1free);
    canard.node_id = (uint8_t)serial_number;

    canard_tx_queue = canardTxInit(CANARD_TX_QUEUE_SIZE, CANARD_MTU_CAN_CLASSIC);

    /* This must be last because it's what's used to detect if everything has been initialized */
    canard_mutex = xSemaphoreCreateBinaryStatic(&canard_mutex_buffer);
    xSemaphoreGive(canard_mutex);

#if IS_MASTER_NODE
    reset_all_cards();
    CANARD_SUBSCRIBE_FIXED_ID(uavcan_sharp_network_Ready_1_0);
#else
    CANARD_SUBSCRIBE_FIXED_ID(uavcan_sharp_network_Reboot_1_0);
#endif

    for (;;)
    {
        uavcan_node_Heartbeat_1_0 heartbeat = {.uptime = HAL_GetTick(),
                                               .health = {uavcan_node_Health_1_0_NOMINAL},
                                               .mode = {uavcan_node_Mode_1_0_OPERATIONAL}};
        CANARD_PUBLISH_MULTICAST_FIXED_ID(uavcan_node_Heartbeat_1_0, CanardPriorityNominal, heartbeat);
        sleep(100);
        process_canard_TX_queue();
        process_canard_RX_queue();

#if !IS_MASTER_NODE
        monitor_network_commands();
#endif
    }
}
