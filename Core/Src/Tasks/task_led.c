#include "Tasks/task_led.h"
#include <cmsis_os2.h>
#include <stm32f4xx_hal_gpio.h>

#include "logs.h"

_Noreturn void vStartLedTask(void *parameters)
{
    struct vLEDTaskParameters *param = (struct vLEDTaskParameters *)parameters;
    TickType_t xLastWakeTime = xTaskGetTickCount();

    for (;;)
    {
        HAL_GPIO_TogglePin(param->gpio_port, param->gpio_pin);
        LOG_DEBUG("Heartbeat");
        vTaskDelayUntil(&xLastWakeTime, LED_BLINK_MS);
    }
}
