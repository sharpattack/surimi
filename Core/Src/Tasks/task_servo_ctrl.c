#include "Tasks/task_servo_ctrl.h"

#include "FreeRTOS.h"
#include "Servo/driver.h"
#include "Servo/elevator.h"
#include <cmsis_os2.h>
#include <stdint.h>
#include <stm32_hal_legacy.h>
#include <stm32f4xx_hal_gpio.h>
#include <stm32f4xx_hal_tim.h>

#include "logs.h"
#include "main.h"
#include "tim.h"

typedef enum
{
    RETRACT = 0,
    EXTEND = 1,
    OPEN = 2,
    CLOSE = 3,
    TURN_SOLAR_YELLOW = 4,
    TURN_SOLAR_BLUE = 5,
} Action;

typedef struct
{
    Side side;
    Floor floor;
    Action action;
} Command;

uint16_t command_to_uint(Command command)
{
    uint16_t resp = command.action;
    resp += command.floor << 4;
    resp += command.side << 8;

    return resp;
}

Command uint_to_command(uint16_t command)
{
    Command resp = {
        .action = command & 0x000f,
        .floor = (command & 0x00f0) >> 4,
        .side = (command & 0x0f00) >> 8,
    };
    return resp;
}

_Noreturn void StartServoCtrlTask(void *parameters)
{
    for (;;)
    {
        vTaskDelete(NULL);
        continue;
    }
    TickType_t xLastWakeTime = xTaskGetTickCount();
    /* To delete, deactivate servo */
    Servo_Elevator_init();
    LOG_INFO("Elevator init");

    for (;;)
    {
        vTaskDelayUntil(&xLastWakeTime, 5); // 5 ticks = 5 ms = 200 Hz

        /*
        if (node == NULL)
        {
            continue;
        }
        */

        uint16_t command = 0xFF;
        uint16_t current = 0xFF;
        /*
        CODictRdWord(&node->Dict, CO_DEV(0x2100, 0x2), &current);

        if (CODictRdWord(&node->Dict, CO_DEV(0x2100, 0x1), &command) != CO_ERR_NONE)
        {
            LOG_ERROR("Servo command entry not found");
            continue;
        }
        if (command == current)
        {

            continue;
        }

        */
        // LOG_INFO("commanded 0x%02x", command);
        Command c = uint_to_command(command);
        // CODictWrWord(&node->Dict, CO_DEV(0x2100, 0x2), command);
        switch (c.action)
        {
        case RETRACT:
            Servo_Elevator_retract(c.floor, c.side, 0);
            break;
        case EXTEND:
            Servo_Elevator_extend(c.floor, c.side, 0);
            break;
        case OPEN:
            Servo_Elevator_open(c.floor, c.side, 0);
            break;
        case CLOSE:
            Servo_Elevator_close(c.floor, c.side, 0);
            break;
        case TURN_SOLAR_YELLOW:
            Servo_Elevator_Solar_turn_yellow();
            break;
        case TURN_SOLAR_BLUE:
            Servo_Elevator_Solar_turn_blue();
            break;
        default:
            break;
        }
    }
}
