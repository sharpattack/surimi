#include "FreeRTOSConfig.h"
#include "Macros.h"
#include "TMC5130.h"
#include "TMC5130_Constants.h"
#include "TMC5130_Fields.h"
#include "Types.h"
#include "canard.h"
#include "cmsis_os.h"
#include "elevator.h"
#include "event_groups.h"
#include "logs.h"
#include "main.h"
#include "portmacro.h"
#include "step_motor.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_def.h"
#include "task_canard.h"
#include "tmc/ic/TMC5130/TMC5130_Register.h"
#include "uavcan/sharp/network/Ready_1_0.h"
#include "uavcan/sharp/surimi/actions/MoveElevator_1_0.h"
#include "uavcan/sharp/surimi/elements/Elevator_1_0.h"
#include "uavcan/sharp/surimi/status/ElevatorStatus_1_0.h"
#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#define SPEED (10.0f * M_PI)
#define MIN_ALLOWED_HEIGHT 5

#define _CATCH_ERROR(dest, error_var, no_error, command, ...)                                                          \
    if ((error_var = command) != no_error)                                                                             \
    {                                                                                                                  \
        LOG_ERROR(__VA_ARGS__);                                                                                        \
        goto dest;                                                                                                     \
    };
#define _CATCH_HAL_ERROR(...) _CATCH_ERROR(fatal_error, hal_error, HAL_OK, __VA_ARGS__)

/* Screw pitch : 8mm/rotation */
#define PITCH 8.0f
#define MM_TO_RAD(mm) (float32_t)((float32_t)(mm) * (2.0f * M_PI) / PITCH)
#define RAD_TO_MM(rad) (int16_t)(((rad)*PITCH) / (2.0f * M_PI))

CANARD_PREPARE_PUBLISH_FIXED_ID(uavcan_sharp_surimi_status_ElevatorStatus_1_0);
CANARD_PREPARE_SUBSCRIBE_FIXED_ID(uavcan_sharp_surimi_actions_MoveElevator_1_0, 1);

typedef enum
{
    Idle = 0,
    Working = 1,
    Stopped = 2
} MotorStatus;

static bool initialized = false;

bool stepper_task_is_initialized()
{
    return initialized;
}

MotorStatus get_status(StepMotor motor)
{
    uint32_t rampstat = tmc5130_readInt(motor.handle, TMC5130_RAMPSTAT);
    if (FIELD_GET(rampstat, TMC5130_EVENT_POS_REACHED_MASK,
                  TMC5130_EVENT_POS_REACHED_SHIFT)) /* StepMotor_finished uses rampstat and would clear the interrupt */
    {
        return Idle;
    }
    if (FIELD_GET(rampstat, TMC5130_EVENT_STOP_L_MASK, TMC5130_EVENT_STOP_L_SHIFT) ||
        FIELD_GET(rampstat, TMC5130_EVENT_STOP_R_MASK, TMC5130_EVENT_STOP_R_SHIFT))
    {
        return Stopped;
    }
    return Working;
}

uavcan_sharp_surimi_elements_Elevator_1_0 get_floor_status(StepMotor motor, uint16_t max_height, uint8_t floor)
{
    uint16_t resp = max_height - RAD_TO_MM(StepMotor_get_current_angle(motor));
    return (uavcan_sharp_surimi_elements_Elevator_1_0){
        .floor = floor,
        .height_mm = resp,
        .status = get_status(motor),
    };
}

void process_floor_command(StepMotor motor, uint16_t max_height, uint16_t command)
{
    LOG_DEBUG("Command %d - %d", command, max_height - command);
    if (command <= MIN_ALLOWED_HEIGHT) /* Minimum height limit */
    {
        LOG_ERROR("Desired elevator height of %d should not be higher than %d", command, MIN_ALLOWED_HEIGHT)
        return;
    }
    StepMotor_angle_async(motor, MM_TO_RAD(max_height - command), SPEED);
}

_Noreturn void StartStepMotorTask(void *parameters)
{
    const int16_t FLOOR1_MAX_HEIGHT_MM = 135;
    const int16_t FLOOR2_MAX_HEIGHT_MM = 171;

    TickType_t xLastWakeTime = xTaskGetTickCount();
    HAL_StatusTypeDef hal_error;

    StepMotor_init();
    StepMotor floor_1 = StepMotor_register(CS_MOTOR_1_GPIO_Port, CS_MOTOR_1_Pin, 200); /* Weird but ok */
    StepMotor floor_2 = StepMotor_register(CS_MOTOR_2_GPIO_Port, CS_MOTOR_2_Pin, 200);

    StepMotor_homing(floor_2, false, true, true, MM_TO_RAD(0));  /* Homing by going upwards */

    StepMotor_homing(floor_1, false, true, false, MM_TO_RAD(0)); /* Homing by going upwards */

    /* Lower down the lowest floor to get some space for retracting claws */
    StepMotor_angle(floor_1, MM_TO_RAD(FLOOR1_MAX_HEIGHT_MM - 120), SPEED, portMAX_DELAY);

    CANARD_SUBSCRIBE_FIXED_ID(uavcan_sharp_surimi_actions_MoveElevator_1_0);

    initialized = true;
    LOG_INFO("Motors initialized");

    ready_for_feature(uavcan_sharp_network_Ready_1_0_FEATURE_ELEVATOR);

    QueueHandle_t orders = CANARD_GET_QUEUE_FIXED_ID(uavcan_sharp_surimi_actions_MoveElevator_1_0);

    for (;;)
    {
        vTaskDelayUntil(&xLastWakeTime, 200); // 100 ticks = 100 ms

        /* Process incoming requests */
        uavcan_sharp_surimi_actions_MoveElevator_1_0 order = {};
        while (xQueueReceive(orders, &order, 0))
        {
            switch (order.floor)
            {
            case 1:
                LOG_INFO("Floor 1 at %d", order.height_mm);
                process_floor_command(floor_1, FLOOR1_MAX_HEIGHT_MM, order.height_mm);
                break;
            case 2:
                LOG_INFO("Floor 1 at %d", order.height_mm);
                process_floor_command(floor_2, FLOOR2_MAX_HEIGHT_MM, order.height_mm);
                break;
            default:
                LOG_WARNING("Unknown elevator floor received %d", order.floor);
            }
        }

        /* Broadcast elevator status */
        uavcan_sharp_surimi_status_ElevatorStatus_1_0 status = {.registered = 2,
                                                                .elevators = {
                                                                    get_floor_status(floor_1, FLOOR1_MAX_HEIGHT_MM, 1),
                                                                    get_floor_status(floor_2, FLOOR2_MAX_HEIGHT_MM, 2),
                                                                }};
        CANARD_PUBLISH_MULTICAST_FIXED_ID(uavcan_sharp_surimi_status_ElevatorStatus_1_0, CanardPriorityNominal, status);
    }
}
