#include "main.h"
#include "spi.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_def.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_spi.h"

#include "FreeRTOS.h" // for task delay ...
#include "logs.h"
#include "motors.h"
#include "task.h"
#include <cmsis_gcc.h>

static StepMotorChannel step_motor_channels[N_MAX_STEP_MOTOR];
static uint8_t nb_step_motors = 0;

BaseType_t step_motor_init(TMC5130TypeDef *tmc5130, StepMotorChannel channel, ConfigurationTypeDef *config)
{
    if (nb_step_motors >= N_MAX_STEP_MOTOR)
    {
        LOG_ERROR("Already using max number of step motor %d", N_MAX_STEP_MOTOR);
        return pdFAIL;
    }
    step_motor_channels[nb_step_motors] = channel;
    tmc5130_init(tmc5130, nb_step_motors, config, tmc5130_defaultRegisterResetState);
    nb_step_motors++;

    return pdPASS;
}

void tmc5130_readWriteArray(uint8_t channel, uint8_t *data, size_t length)
{
    HAL_StatusTypeDef status;
    uint8_t tx[5] = {0, 0, 0, 0, 0}; // Datagram is always 40 bits

    if (length > 5)
    {
        LOG_ERROR("You can't send more than 40 bits over SPI for a TMC5130, you are sending %d bits", length * 8);

        while (1)
        {
            vTaskDelay(100);
        }
    }

    for (size_t i = 0; i < length; i++)
    {
        tx[i] = data[i];
    }

    HAL_GPIO_WritePin(step_motor_channels[channel].gpio, step_motor_channels[channel].pin, GPIO_PIN_RESET);
    status = HAL_SPI_TransmitReceive(&hspi1, tx, data, 5, 10);
    HAL_GPIO_WritePin(step_motor_channels[channel].gpio, step_motor_channels[channel].pin, GPIO_PIN_SET);

    if (status != HAL_OK)
    {
        LOG_ERROR("Could not send message to TMC5130");
        while (1)
        {
            vTaskDelay(100);
        }
    }
    LOG_DEBUG(
        "Channel: %d\r\n Sent: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n Received: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x",
        channel, tx[0], tx[1], tx[2], tx[3], tx[4], data[0], data[1], data[2], data[3], data[4]);
}
