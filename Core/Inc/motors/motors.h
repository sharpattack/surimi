#ifndef MOTORS_H
#define MOTORS_H
#include "TMC5130.h"
#include "main.h"
#include "portmacro.h"
#include "projdefs.h"
#include "stm32f446xx.h"
#include <stdint.h>

#define N_MOTOR 2
#define N_MAX_STEP_MOTOR 3

#ifdef __cplusplus
extern "C"
{
#endif

    typedef struct
    {
        GPIO_TypeDef *gpio;
        uint16_t pin;
    } StepMotorChannel;

    void tmc5130_readWriteArray(uint8_t channel, uint8_t *data, size_t length);
    BaseType_t step_motor_init(TMC5130TypeDef *tmc5130, StepMotorChannel channel, ConfigurationTypeDef *config);
#ifdef __cplusplus
}
#endif

#endif
