/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2024 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

    /* Private includes ----------------------------------------------------------*/
    /* USER CODE BEGIN Includes */

    /* USER CODE END Includes */

    /* Exported types ------------------------------------------------------------*/
    /* USER CODE BEGIN ET */

    /* USER CODE END ET */

    /* Exported constants --------------------------------------------------------*/
    /* USER CODE BEGIN EC */

    /* USER CODE END EC */

    /* Exported macro ------------------------------------------------------------*/
    /* USER CODE BEGIN EM */

    /* USER CODE END EM */

    /* Exported functions prototypes ---------------------------------------------*/
    void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SOLAR_PANEL_1_Pin GPIO_PIN_1
#define SOLAR_PANEL_1_GPIO_Port GPIOA
#define SOLAR_PANEL_2_Pin GPIO_PIN_2
#define SOLAR_PANEL_2_GPIO_Port GPIOA
#define SOLAR_PANEL_3_Pin GPIO_PIN_3
#define SOLAR_PANEL_3_GPIO_Port GPIOA
#define RACK_LEFT_2_Pin GPIO_PIN_6
#define RACK_LEFT_2_GPIO_Port GPIOA
#define CLAW_LEFT_2_Pin GPIO_PIN_7
#define CLAW_LEFT_2_GPIO_Port GPIOA
#define RACK_RIGHT_2_Pin GPIO_PIN_0
#define RACK_RIGHT_2_GPIO_Port GPIOB
#define CLAW_RIGHT_2_Pin GPIO_PIN_1
#define CLAW_RIGHT_2_GPIO_Port GPIOB
#define TIMER_TEST_FREQ_Pin GPIO_PIN_2
#define TIMER_TEST_FREQ_GPIO_Port GPIOB
#define RACK_LEFT_1_Pin GPIO_PIN_9
#define RACK_LEFT_1_GPIO_Port GPIOE
#define CLAW_LEFT_1_Pin GPIO_PIN_11
#define CLAW_LEFT_1_GPIO_Port GPIOE
#define RACK_RIGHT_1_Pin GPIO_PIN_13
#define RACK_RIGHT_1_GPIO_Port GPIOE
#define CLAW_LEFT_1E14_Pin GPIO_PIN_14
#define CLAW_LEFT_1E14_GPIO_Port GPIOE
#define RACK_LEFT_3_Pin GPIO_PIN_12
#define RACK_LEFT_3_GPIO_Port GPIOD
#define CLAW_LEFT_3_Pin GPIO_PIN_13
#define CLAW_LEFT_3_GPIO_Port GPIOD
#define RACK_RIGHT_3_Pin GPIO_PIN_14
#define RACK_RIGHT_3_GPIO_Port GPIOD
#define CLAW_RIGHT_3_Pin GPIO_PIN_15
#define CLAW_RIGHT_3_GPIO_Port GPIOD
#define CS_MOTOR_1_Pin GPIO_PIN_8
#define CS_MOTOR_1_GPIO_Port GPIOA
#define CS_MOTOR_2_Pin GPIO_PIN_9
#define CS_MOTOR_2_GPIO_Port GPIOA
#define CS_MOTOR_3_Pin GPIO_PIN_10
#define CS_MOTOR_3_GPIO_Port GPIOA
#define LED_HEARTBEAT_Pin GPIO_PIN_4
#define LED_HEARTBEAT_GPIO_Port GPIOD

    /* USER CODE BEGIN Private defines */

    /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
