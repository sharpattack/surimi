#ifndef STEP_MOTOR_H_INCLUDED
#define STEP_MOTOR_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

#include "FreeRTOS.h"
#include "Types.h"
#include "main.h"
#include "motors.h"
#include <stdbool.h>
#include <stdint.h>

#define MAX_STEPPER_MOTORS N_MAX_STEP_MOTOR

    /* Descriptor of a stepper motor */
    typedef struct
    {
        int8_t index;
        TMC5130TypeDef *handle;
    } StepMotor;

    /**
     * Initialize the library
     *
     * @return success status of the function
     **/
    HAL_StatusTypeDef StepMotor_init();

    /**
     * Un-initialize the library
     **/
    void StepMotor_deinit();

    /**
     * Configure a stepper motor according to it's characteristics
     *
     * @param gpio_channel       gpio of the NSS pin for SPI
     * @param pin_dir            pin of the NSS pin for SPI
     * @param stpes_for_one_turn total number of steps for one full turn without micro-stepping
     *
     * @return descriptor for the registered motor, index will be -1 and handle NULL if it fails
     **/
    StepMotor StepMotor_register(GPIO_TypeDef *gpio_channel, uint16_t pin_channel, uint32_t steps_for_one_turn);

    /**
     * Un-register a stepper motor and removes it from the configuration
     *
     * @param motor descriptor of the motor
     **/
    void StepMotor_unregister(StepMotor motor);

    /**
     * Trigger homing procedure
     *
     * @param motor handle to the motor
     * @param forward homing direction (as boolean)
     * @param sensor_left enable sensor left (as boolean)
     * @param sensor_right enable sensor right (as boolean)
     * @param initial_angle initial absolute angle position in rad after homing procedure
     *
     * @return status of the command
     **/
    HAL_StatusTypeDef StepMotor_homing(StepMotor motor, bool forward, bool sensor_left, bool sensor_right,
                                       float32_t initial_angle);

    /**
     * Return the current angle of the motor in rad/s
     *
     * @param motor handle to the motor
     *
     * @return current angle in rad/s, returns 0 if the motor doesn't exist
     */
    float32_t StepMotor_get_current_angle(StepMotor motor);

    /**
     * Compute the maximum speed of the motor in rad/s
     *
     * @param motor handle to the motor
     *
     * @return maximum speed in rad/s, returns 0 if the motor doesn't exist
     */
    float32_t StepMotor_max_speed(StepMotor motor);

    /**
     * Helper function to convert a tick speed to an angular speed
     *
     * @param motor handle to motor
     * @param speed steps per seconds
     *
     * @return speed of for this motor in rad/s
     **/
    float32_t StepMotor_step_speed_to_angle(StepMotor motor, uint32_t speed);

    /**
     * Turn a stepper motor at a given angle in rad
     * The position is absolute
     * Wait for the command to complete
     *
     * @param motor handle to the motor
     * @param angle in rad
     * @param speed in rad/s
     * @param timeout in ticks
     *
     * @return status of the command
     **/
    HAL_StatusTypeDef StepMotor_angle(StepMotor motor, float32_t angle, float32_t speed, TickType_t timeout);

    /**
     * Turn a stepper motor at a given angle in rad
     * The position is absolute
     * Don't wait for the command to complete
     *
     * @param motor handle to the motor
     * @param angle in rad
     * @param speed in rad/s
     *
     * @return status of the command
     **/
    HAL_StatusTypeDef StepMotor_angle_async(StepMotor motor, float32_t angle, float32_t speed);

    HAL_StatusTypeDef StepMotor_wait(StepMotor motor, TickType_t timeout);

    HAL_StatusTypeDef _StepMotor_wait_n(StepMotor *motors, TickType_t timeout);

    bool StepMotor_finished(StepMotor motor);

#define StepMotor_wait_n(timeout, ...)                                                                                 \
    {                                                                                                                  \
        StepMotor motors[] = {__VA_ARGS__, (StepMotor){.handle = NULL}};                                               \
        _StepMotor_wait_n(motors, timeout);                                                                            \
    }

#ifdef __cplusplus
}
#endif

#endif // STEP_MOTOR_H_INCLUDED
