#ifndef TASK_STEP_MOTOR_H_INCLUDED
#define TASK_STEP_MOTOR_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

#include "step_motor.h"

    _Noreturn void StartStepMotorTask(void *parameters);
    bool stepper_task_is_initialized();

#ifdef __cplusplus
}
#endif

#endif // TASK_STEP_MOTOR_H_INCLUDED
