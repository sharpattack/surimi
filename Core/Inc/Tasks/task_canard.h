#ifndef TASK_CANARD_H_INCLUDED
#define TASK_CANARD_H_INCLUDED

#include <stdbool.h>
#ifdef __cplusplus
extern "C"
{
#endif
#include "FreeRTOS.h"
#include "canard.h"
#include "logs.h"
#include "queue.h"

#ifndef MAX_FEATURES
#define MAX_FEATURES 5
#endif

#ifndef IS_MASTER_NODE
#define IS_MASTER_NODE false
#endif

    _Noreturn void StartCanardTask(void *parameters);

    int32_t _publish_canard_payload(uint8_t *payload, size_t payload_size, CanardTransferMetadata *metadata);
    int8_t _subsrcibe_canard_topic(const CanardPortID port_id, const size_t extent,
                                   CanardRxSubscription *const out_subscription);

#if IS_MASTER_NODE
    bool wait_for_feature(uint8_t feature, TickType_t timeout);
#else
void ready_for_feature(uint8_t feature);
#endif

#define _publish_topic(id, type) _publish_##id##_##type
#define _queue_handle(id, type) _subscribe_##id##_##type##_queue
#define _queue_buffer(id, type) _subscribe_##id##_##type##_queue_buffer
#define _queue_storage(id, type) _subscribe_##id##_type##_storage
#define _subscription(id, type) _subscription_##id##_##type
#define _deserialize_callback(id, type) _deserialize_##id##_##type
#define _get_topic_queue(id, type) _subscribe_get_or_init_##id##_##type()

#define CANARD_PREPARE_PUBLISH(id, type)                                                                               \
    void _publish_topic(id, type)(type * message, CanardTransferMetadata * metadata)                                   \
    {                                                                                                                  \
        static uint8_t counter = 0;                                                                                    \
        size_t payload_size = type##_EXTENT_BYTES_;                                                                    \
        uint8_t payload[type##_EXTENT_BYTES_];                                                                         \
        type##_serialize_(message, payload, &payload_size);                                                            \
        metadata->transfer_id = counter;                                                                               \
        if (_publish_canard_payload(                                                                                   \
                payload,                                                                                               \
                type##_EXTENT_BYTES_, /* We don't use payload_size as it could have been modified by serialize */      \
                metadata) > 0)                                                                                         \
        {                                                                                                              \
            counter++;                                                                                                 \
        }                                                                                                              \
        else                                                                                                           \
        {                                                                                                              \
            LOG_WARNING("Could not send message " #type "  with id " #id ", queue is full");                           \
        }                                                                                                              \
    }
#define CANARD_PREPARE_PUBLISH_FIXED_ID(type) CANARD_PREPARE_PUBLISH(type##_FIXED_PORT_ID_, type)

#define CANARD_PUBLISH(id, type, message_priority, transfer_type, destination_node, message)                           \
    {                                                                                                                  \
        CanardTransferMetadata metadata = {                                                                            \
            .priority = message_priority,                                                                              \
            .transfer_kind = transfer_type,                                                                            \
            .port_id = id,                                                                                             \
            .remote_node_id = destination_node,                                                                        \
        };                                                                                                             \
        _publish_topic(id, type)(&message, &metadata);                                                                 \
    }
#define CANARD_PUBLISH_FIXED_ID(type, ...) CANARD_PUBLISH(type##_FIXED_PORT_ID_, type, __VA_ARGS__)

#define CANARD_PUBLISH_MULTICAST(id, type, message_priority, message)                                                  \
    CANARD_PUBLISH(id, type, message_priority, CanardTransferKindMessage, CANARD_NODE_ID_UNSET, message)
#define CANARD_PUBLISH_MULTICAST_FIXED_ID(type, ...) CANARD_PUBLISH_MULTICAST(type##_FIXED_PORT_ID_, type, __VA_ARGS__)

#define CANARD_GET_QUEUE(id, type) _get_topic_queue(id, type)
#define CANARD_GET_QUEUE_FIXED_ID(type) CANARD_GET_QUEUE(type##_FIXED_PORT_ID_, type)

#define CANARD_PREPARE_SUBSCRIBE(id, type, rx_size)                                                                    \
    QueueHandle_t _queue_handle(id, type) = NULL;                                                                      \
    static StaticQueue_t _queue_buffer(id, type);                                                                      \
    uint8_t _queue_storage(id, type)[rx_size * sizeof(type)];                                                          \
    CanardRxSubscription _subscription(id, type);                                                                      \
    QueueHandle_t CANARD_GET_QUEUE(id, type)                                                                           \
    {                                                                                                                  \
        if (_queue_handle(id, type) == NULL)                                                                           \
        {                                                                                                              \
            _queue_handle(id, type) =                                                                                  \
                xQueueCreateStatic(rx_size, sizeof(type), _queue_storage(id, type), &_queue_buffer(id, type));         \
        }                                                                                                              \
        if (_queue_handle(id, type) == NULL)                                                                           \
        {                                                                                                              \
            LOG_ERROR("Could not init queue for type " #type " with id %d", id);                                       \
            for (;;)                                                                                                   \
            {                                                                                                          \
            }                                                                                                          \
        }                                                                                                              \
        return _queue_handle(id, type);                                                                                \
    }                                                                                                                  \
    void _deserialize_callback(id, type)(void *parameters)                                                             \
    {                                                                                                                  \
        CanardRxTransfer *transfer = (CanardRxTransfer *)parameters;                                                   \
        type deserialized = {};                                                                                        \
        size_t size = type##_EXTENT_BYTES_;                                                                            \
        type##_deserialize_(&deserialized, transfer->payload, &size);                                                  \
        QueueHandle_t queue = CANARD_GET_QUEUE(id, type);                                                              \
        if (rx_size > 1)                                                                                               \
        {                                                                                                              \
            xQueueSendToBack(queue, &deserialized, 0);                                                                 \
        }                                                                                                              \
        else                                                                                                           \
        {                                                                                                              \
            xQueueOverwrite(queue, &deserialized);                                                                     \
        }                                                                                                              \
    }
#define CANARD_PREPARE_SUBSCRIBE_FIXED_ID(type, rx_size) CANARD_PREPARE_SUBSCRIBE(type##_FIXED_PORT_ID_, type, rx_size)

#define CANARD_SUBSCRIBE(id, type)                                                                                     \
    _subsrcibe_canard_topic(id, type##_EXTENT_BYTES_, &_subscription(id, type));                                       \
    _subscription(id, type).user_reference = _deserialize_callback(id, type);

#define CANARD_SUBSCRIBE_FIXED_ID(type) CANARD_SUBSCRIBE(type##_FIXED_PORT_ID_, type)

#ifdef __cplusplus
}
#endif

#endif // TASK_CANARD_H_INCLUDED
