#ifndef LOGS_H_INCLUDED
#define LOGS_H_INCLUDED

#ifndef LOG_LEVEL
#error LOG_LEVEL is not defined
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#include "FreeRTOS.h"
#include "SEGGER/SEGGER_RTT.h"
#include "cmsis_os.h"
#include "projdefs.h"
#include "stm32f4xx_hal.h"
#include <stdint.h>

    void init_logs();
    void log_fatal_error(const char *type, xTaskHandle task, signed char *pcTaskName);

#define LOGS_RTT_CHANNEL 0
#define SEND_BUFFER_SIZE 1024

#define _MESSAGE_DEBUG "\n\rDEBUG: "

#define _MESSAGE_INFO "\n\rINFO: "

#define _MESSAGE_WARNING "\n\rWARNING: "

#define _MESSAGE_ERROR "\n\rERROR: "

#define _MESSAGE_LEVEL(LEVEL) _MESSAGE##LEVEL
#define _MESSAGE_LEVEL_SIZE(LEVEL) _MESSAGE_SIZE##LEVEL

#define SEGGER_LOG_PRINT(LOGS_RTT_CHANNEL, ...)                                                              \
{                                                                                                            \
    char tmpBuff__[255] = "";                                                                                \
    int nbWritten__ = snprintf(tmpBuff__, sizeof(tmpBuff__), __VA_ARGS__);                                   \
    if (nbWritten__ > 0)                                                                                     \
    {                                                                                                        \
        SEGGER_RTT_Write((LOGS_RTT_CHANNEL), tmpBuff__, LL_MIN(sizeof(tmpBuff__), (uint32_t)(nbWritten__))); \
    }                                                                                                        \
}

#define _LOG_GENERIC(LEVEL, ORIGIN, COLOR, ...) \
    SEGGER_LOG_PRINT(LOGS_RTT_CHANNEL, "%s%s%s%s", RTT_CTRL_RESET, COLOR, _MESSAGE_LEVEL(LEVEL), RTT_CTRL_RESET);     \
    SEGGER_LOG_PRINT(LOGS_RTT_CHANNEL, __VA_ARGS__);

#define LOG_DEBUG(...)
#define LOG_INFO(...)
#define LOG_WARNING(...)
#define LOG_ERROR(...)

#if LOG_LEVEL <= 3
#undef LOG_ERROR
#define LOG_ERROR(...) _LOG_GENERIC(_ERROR, _task, RTT_CTRL_TEXT_BRIGHT_RED, __VA_ARGS__)
#endif
#if LOG_LEVEL <= 2
#undef LOG_WARNING
#define LOG_WARNING(...) _LOG_GENERIC(_WARNING, _task, RTT_CTRL_TEXT_BRIGHT_YELLOW, __VA_ARGS__)
#endif
#if LOG_LEVEL <= 1
#undef LOG_INFO
#define LOG_INFO(...) _LOG_GENERIC(_INFO, _task, RTT_CTRL_TEXT_BRIGHT_CYAN, __VA_ARGS__)
#endif
#if LOG_LEVEL <= 0
#undef LOG_DEBUG
#define LOG_DEBUG(...) _LOG_GENERIC(_DEBUG, _task, RTT_CTRL_TEXT_BRIGHT_GREEN, __VA_ARGS__)
#endif

#define LOG(...) LOG_INFO(__VA_ARGS__)

#define assert_and_log(cond, mess)                                                                                     \
    {                                                                                                                  \
        if (!(cond))                                                                                                   \
        {                                                                                                              \
            LOG_ERROR(mess);                                                                                           \
            taskDISABLE_INTERRUPTS();                                                                                  \
            for (;;)                                                                                                   \
                ;                                                                                                      \
        }                                                                                                              \
    }

#ifdef __cplusplus
}
#endif

#endif // LOGS_H_INCLUDED
