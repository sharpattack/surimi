#ifndef STORAGE_H_INCLUDED
#define STORAGE_H_INCLUDED

#include <stdint.h>
#define NVM_SIM_SIZE 32

typedef struct
{
    uint8_t nodeId;
    uint32_t baudrate;
    uint8_t counter;
    uint8_t nvm_memory[NVM_SIM_SIZE];
} DataStore;

void reset_data_store();
DataStore *get_data_store();

#endif // STORAGE_H_INCLUDED
