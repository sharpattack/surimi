#ifndef SERVO_ELEVATOR_H_INCLUDED
#define SERVO_ELEVATOR_H_INCLUDED

#include "Servo/driver.h"
#include <stdint.h>
#include <stm32f4xx_hal_def.h>

/*
    Duty Cycle:
    < 50% -> Clockwise
    > 50% -> Counter-Clockwise
      50% -> Neutral
    += 3% deadzone
*/

#define MAX_ON_OFF_DELAY 1500 /* 1.5 seconds */

typedef enum
{
    FLOOR_1 = 0,
    FLOOR_2 = 4,
    FLOOR_3 = 8
} Floor;

typedef enum
{
    RACK = 0,
    CLAW = 1
} Type;

typedef enum
{
    LEFT = 0,
    RIGHT = 2
} Side;

typedef enum
{
    CLOCKWISE = 65,
    ANTICLOCKWISE = 40
} Direction;

HAL_StatusTypeDef Servo_Elevator_init();

HAL_StatusTypeDef Servo_Elevator_retract(Floor floor, Side side, uint32_t max_wait);
HAL_StatusTypeDef Servo_Elevator_extend(Floor floor, Side side, uint32_t max_wait);

HAL_StatusTypeDef Servo_Elevator_open(Floor floor, Side side, uint32_t max_wait);
HAL_StatusTypeDef Servo_Elevator_close(Floor floor, Side side, uint32_t max_wait);
void Servo_Elevator_Solar_turn_yellow();
void Servo_Elevator_Solar_turn_blue();

uint8_t Servo_Elevator_is_initialized();

#endif // SERVO_ELEVATOR_H_INCLUDED
