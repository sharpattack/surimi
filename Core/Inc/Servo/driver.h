#ifndef SERVO_DRIVER_H_INCLUDED
#define SERVO_DRIVER_H_INCLUDED

#include "stm32f4xx_hal.h"
#include <stdint.h>
#include <stm32f4xx_hal_def.h>
#include <stm32f4xx_hal_tim.h>

typedef enum
{
    /* Floor 1 */
    FLOOR_1_RACK_LEFT = 0,
    FLOOR_1_CLAW_LEFT = 1,
    FLOOR_1_RACK_RIGHT = 2,
    FLOOR_1_CLAW_RIGHT = 3,
    /* Floor 2 */
    FLOOR_2_RACK_LEFT = 4,
    FLOOR_2_CLAW_LEFT = 5,
    FLOOR_2_RACK_RIGHT = 6,
    FLOOR_2_CLAW_RIGHT = 7,
    /* Floor 3 */
    FLOOR_3_RACK_LEFT = 8,
    FLOOR_3_CLAW_LEFT = 9,
    FLOOR_3_RACK_RIGHT = 10,
    FLOOR_3_CLAW_RIGHT = 11,
    /* Solar panel */
    SOLAR_PANEL_1 = 12,
    SOLAR_PANEL_2 = 13,
    SOLAR_PANEL_3 = 14
} ServoHandle_t;

/**
 * @brief Interrupt hook needed to perform motor control in background
 * @param htim TIM base handle
 * @retval None
 **/
void Servo_Driver_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

/**
 * @brief Initialize servo motor library and set all duty cycles to 0
 * @retval HAL status
 **/
HAL_StatusTypeDef Servo_init();

/**
 * @brief Set the duty cycle of a servo motor
 * @param Servo motor handle
 * @param duty cycle value from 0 to 100
 * @param time in ms to apply this pwm for
 * @param maximum amount of time to wait for the command to finish
 * @retval HAL status
 **/
HAL_StatusTypeDef Servo_set_duty_cycle(ServoHandle_t handle, uint8_t duty_cycle, uint32_t time, uint32_t max_wait);

/**
 * @brief wait for a command to finish
 * @param Servo motor handle of the motor to wait for
 * @param maximum amount of time to wait for the command to finish
 * @retval HAL status
 **/
HAL_StatusTypeDef Servo_wait(ServoHandle_t handle, uint32_t max_wait);

#endif // SERVO_DRIVER_H_INCLUDED
