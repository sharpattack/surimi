#!/usr/bin/env python3
import argparse


def convert(input_file, output_file, name):
    banner = (
        '"'
        + str(("\n\r" + input_file.read().replace("\n", "\n\r")).encode("utf-8"))[2:-1]
        + '"'
    )

    output_file.write(f"#ifndef {name.upper()}_H_INCLUDED\n")
    output_file.write(f"#define {name.upper()}_H_INCLUDED\n\n")
    output_file.write(f"#include <stdint.h>\n\n")

    output_file.write(f"const uint8_t {name}[] = {banner};\n\n")

    output_file.write(f"#endif // {name.upper()}_H_INCLUDED\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert banner .txt file to .h file")
    parser.add_argument(
        "input_file", type=argparse.FileType("r"), help="Input .txt file"
    )
    parser.add_argument(
        "output_file", type=argparse.FileType("w+"), help="Output .h file"
    )
    parser.add_argument(
        "--name", type=str, help="Name for the variable", default="header"
    )

    args = parser.parse_args()
    convert(args.input_file, args.output_file, args.name)
