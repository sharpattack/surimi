# Set config for vcpkg
if(NOT UNIX)
    message(FATAL_ERROR "This can only build on unix based systems")
endif()

if(FreeBSD)
    message(FATAL_ERROR "We can not compile on FreeBSD")
endif()

set(TRIPLET_SYSTEM "linux")
if(APPLE)
    set(TRIPLET_SYSTEM "osx")
endif()

EXECUTE_PROCESS( COMMAND uname -m COMMAND tr -d '\n' OUTPUT_VARIABLE ARCHITECTURE )
if(ARCHITECTURE STREQUAL "x86_64")
    set(TRIPLET_ARCH "x64")
elseif(ARCHITECTURE MATCHES "arm|aarch64")
    set(TRIPLET_ARCH "arm")
else()
    message(FATAL_ERROR "Architucture ${ARCHITECTURE} is not supported")
endif()

set(VCPKG_TARGET_TRIPLET "${TRIPLET_ARCH}-${TRIPLET_SYSTEM}")

include(${CMAKE_SOURCE_DIR}/cmake/tools/automate-vcpkg.cmake)

vcpkg_bootstrap()
