set(PYTHON_VENV_PATH ${CMAKE_CURRENT_BINARY_DIR}/python_venv)
set(PYTHON_VENV_BIN_PATH ${PYTHON_VENV_PATH}/bin)
set(Python3_VENV_EXECUTABLE ${PYTHON_VENV_BIN_PATH}/python3)
set(Pip3_VENV_EXECUTABLE ${Python3_VENV_EXECUTABLE} -m pip)

macro(make_python3_venv)
  if (NOT EXISTS ${PYTHON_VENV_PATH})
    message("Generating Python 3 venv directory ${PYTHON_VENV_PATH}")
    find_package(Python3 COMPONENTS Interpreter REQUIRED)
    execute_process(
      COMMAND ${Python3_EXECUTABLE} -m venv ${PYTHON_VENV_PATH}
      RESULT_VARIABLE VENV_RESULT
    )

    if (NOT ${VENV_RESULT} STREQUAL "0")
      message(FATAL_ERROR "Error creating python virtualenv. Do you have the venv package insatlled for python ?")
    endif()

    set(ENV{VIRTUAL_ENV} ${PYTHON_VENV_PATH})
    set(Python3_FIND_VIRTUALENV FIRST)
    unset(ENV{PYTHONHOME})
    find_package(Python3 COMPONENTS Interpreter)
  endif()
endmacro()

macro(python3_venv_pip_install package)
  string(SHA256 PACKAGE_HASH ${package})
  if (NOT EXISTS ${PYTHON_VENV_PATH}/${PACKAGE_HASH})
    message(STATUS "Installing ${package}")
    execute_process(COMMAND ${Pip3_VENV_EXECUTABLE} install ${package})
    file(WRITE ${PYTHON_VENV_PATH}/${PACKAGE_HASH} ${package})
  else ()
    message(STATUS "${package} already installed")
  endif ()
endmacro()
