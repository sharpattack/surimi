include(FetchContent)

set(o1heap_VERSION 2.1.1)
set(libcanard_VERSION 3.2.0)

message(STATUS "Fetching third party libraries")
#======================================

FetchContent_Declare(
        o1heap
        GIT_REPOSITORY https://github.com/pavel-kirienko/o1heap.git
        GIT_TAG        ${o1heap_VERSION}
        GIT_SHALLOW    YES
        DOWNLOAD_EXTRACT_TIMESTAMP FALSE
)

FetchContent_Declare(
        libcanard
        GIT_REPOSITORY https://github.com/OpenCyphal/libcanard.git
        GIT_TAG        ${libcanard_VERSION}
        GIT_SHALLOW    YES
        DOWNLOAD_EXTRACT_TIMESTAMP FALSE
)
add_subdirectory(cyphal)
FetchContent_MakeAvailable(o1heap libcanard)

# Install python packages
make_python3_venv()
python3_venv_pip_install("git+https://gitlab.com/sharpattack/rtt-viewer.git@771d5e381e7353ec273ed290b7385f4d98a7a0c8")
python3_venv_pip_install("git+https://gitlab.com/sharpattack/etendard.git@c90e6e90832626ced6efb1bef5a4f5ac07eb011d")
python3_venv_pip_install("nunavut==2.3.1")

#======================================
message(STATUS "Fetching thirdparty libraries done")
